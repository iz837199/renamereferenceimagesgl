import os

def rename_files_in_folder(folder_path):
    
    # create a list of all files in the folder
    filenames = os.listdir(folder_path)

    # sort the filenames
    filenames.sort()

    # loop over all files
    for i, filename in enumerate(filenames):
        # create the new filename
        new_filename = f"Ref_{str(i).zfill(2)}{os.path.splitext(filename)[1]}"

        # create the new filepath
        old_file_path = os.path.join(folder_path, filename)
        new_file_path = os.path.join(folder_path, new_filename)

        # rename the file
        os.rename(old_file_path, new_file_path)

# call the rename function
rename_files_in_folder(r"C:\Users\David_UoR\OneDrive - University of Reading\CSMPR21_data\reference_images")
